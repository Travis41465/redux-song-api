import React from 'react';
import ReactDOM from 'react-dom';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import activeSong from './reducers'
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { combineReducers } from 'redux'
import promiseMiddleware from 'redux-promise';

const createAppReducer = combineReducers({ activeSong: activeSong })
const middleWare = applyMiddleware(promiseMiddleware)
let store = createStore(createAppReducer, middleWare)

render(
    <Provider store={store}>
    <App/>
    </Provider>,
    document.getElementById('root')
)
