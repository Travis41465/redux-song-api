import { connect } from 'react-redux'
import Lyrics from '../components/Lyrics'


const mapStateToProps = state => {
        return {
            lyrics : state.activeSong
        }
}

const GetSong = connect(mapStateToProps, undefined)(Lyrics)

export default GetSong