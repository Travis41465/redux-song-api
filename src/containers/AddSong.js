import { connect } from 'react-redux'
import { addSong } from '../actions'
import Form from '../components/Form'


const mapDispatchToProps = dispatch => {
    return {
        onClick : ({title, artist}) => { 
            dispatch(addSong({title, artist}))
        }
    }
}

const AddSong = connect(undefined, mapDispatchToProps)(Form)

export default AddSong

