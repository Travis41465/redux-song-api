export const addSong = ({title, artist}) => {
    let url = "https://api.lyrics.ovh/v1/" + artist + "/" + title;
    return fetch(url).then((response) => response.json()).then((data) => data.lyrics).then((lyrics) => {
        return receiveSong(lyrics);
    })
}

export const receiveSong = (lyrics) => {
    return {
     type : "ADD_SONG",
     lyrics
    }
}