const initialState = ""
const activeSong = (state=initialState, action) => {
    switch(action.type) {
        case 'ADD_SONG' : 
        return action.lyrics
        default :
        return state;
    }
}

export default activeSong;