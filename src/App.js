import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Form from './components/Form'
import AddSong from './containers/AddSong'
import GetSong from './containers/GetSong'

class App extends Component {
  render() {
    return (
      <div>
      <AddSong/>
      <GetSong/>
      </div>
    );
  }
}

export default App;
