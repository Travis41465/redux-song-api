import React from 'react'

const Form = ({onClick}) => {
    let artistText
    let titleText
    

    return(
    <div>
        <form onSubmit={(e) => {
            e.preventDefault()
            onClick({
                    title : titleText.value,
                    artist : artistText.value
            })
        }            
        }>
            <input 
                ref={node => {
                artistText = node
            }}
            />

          <input
            ref={node => {
                titleText = node
            }}
            />
            <button type="submit">
                Get Lyrics
            </button>
        </form>
    </div>
    )
}


export default Form